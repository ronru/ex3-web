package ex3.demo.Java;


/**
 * The type Response transfer is the object that the controller return
 * when the user pressed on delete button, this object return as a json file
 * and his goal to keep the pages, if the session expired or deleted we set that the user logged out
 */
public class ResponseTransfer {

    private boolean userLogOut;

    /**
     * Gets user log out.
     *
     * @return the user log out ot not
     */
    public boolean getUserLogOut() {
        return userLogOut;
    }

    /**
     * Sets user log out.
     *
     * @param userLogOut  if the user log out
     */
    public void setUserLogOut(boolean userLogOut) {
        this.userLogOut = userLogOut;
    }


}
