package ex3.demo.filters;


import ex3.demo.beans.DataBaseSessionMng;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Logging filter, checks if the session not expired, protect on the pages
 */
@Component
public class UserLoggingFilter implements HandlerInterceptor {

    private DataBaseSessionMng dataBaseSessionMng;


    /**
     * Instantiates a new User logging filter.
     *
     * @param dataBaseSessionMng the data base session mng
     */
    public UserLoggingFilter(DataBaseSessionMng dataBaseSessionMng) {
        this.dataBaseSessionMng = dataBaseSessionMng;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

//if there is no session, redirect to login page
        if (!dataBaseSessionMng.isUserLoggedIn())
            response.sendRedirect("/");

        return true;
    }
}