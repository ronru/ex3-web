package ex3.demo.beans;


import ex3.demo.repo.User;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * This bean in charge to save on the pages safe, when the user logged out this
 * bean are charge to return the user to login by set user not logged in
 */
@Component
public class DataBaseSessionMng implements Serializable {
    private boolean userLoggedIn = false;
    private ArrayList<User> users;

    /**
     * Get users array list.
     *
     * @return the array list
     */
    public ArrayList<User> getUsers(){
        return this.users = new ArrayList<>();
    }

    /**
     * Sets user logged in.
     */
    public void setUserLoggedIn() {
        userLoggedIn = true;
    }

    /**
     * check if user logged in boolean.
     *
     * @return the boolean
     */
    public boolean isUserLoggedIn() {
        return userLoggedIn;
    }

}
