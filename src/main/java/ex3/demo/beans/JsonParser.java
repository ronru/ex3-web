package ex3.demo.beans;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * This Json parser receive json file and parse it.
 * the data return from github, json is the object that searched (github user)
 * json parser parse his login name , no.of followers and his url page
 */
@Component
public class JsonParser {

    //if not user not found
    private boolean userFound;
    //login name
    private String loginName;
    //user followers
    private String userFollowers;
    //user id for db
    private long userId;
    //user html url
    private String userHtmlUrl;
    //the line that return
    private static String inputLine;


    /**
     * Read from github and parse it to string
     *
     * @param userName the user name from the search line
     */
    public void readFromUrl(String userName) {

        try {
            URL oracle = new URL("https://api.github.com/users/" + userName);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            inputLine = in.readLine();
            if (inputLine != null) {
                userFound = true;
                parseJson(inputLine);
            }
            in.close();
        } catch (IOException msg) {
            userFound = false;
        }
    }
    /**
     * Read from github and parse the data and get the strings and enter them
     * to var class
     * @param inputLine is the
     */
    private void parseJson(String inputLine) {
        try {

            JSONObject jsonObject = new JSONObject(inputLine);
            userId =  Long.parseLong(jsonObject.getString("id"));
            loginName = jsonObject.getString("login");
            userFollowers = jsonObject.getString("followers");
            userHtmlUrl = jsonObject.getString("html_url");

        } catch (JSONException msg) {
        }

    }

    /**
     * Gets login name.
     *
     * @return the login name
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Gets user followers.
     *
     * @return the user followers
     */
    public String getUserFollowers() {
        return userFollowers;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Gets user html url.
     *
     * @return the user html url
     */
    public String getUserHtmlUrl() {
        return userHtmlUrl;
    }

    /**
     * Is user found boolean.
     *
     * @return if the user found
     */
    public boolean isUserFound() {
        return userFound;
    }
}
