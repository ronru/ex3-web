package ex3.demo;

import ex3.demo.beans.DataBaseSessionMng;
import ex3.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

/**
 * The type Demo application.
 */
@SpringBootApplication
public class DemoApplication {


    /**
     * The User repository.
     */
    @Autowired
    public UserRepository userRepository;

    /**
     * Session mng data base session mng.
     *
     * @return the data base session mng
     */
    /* we declare a bean to be created by spring in each user session */
    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public DataBaseSessionMng sessionMng() {
        return new DataBaseSessionMng();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
