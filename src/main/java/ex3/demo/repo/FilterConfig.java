package ex3.demo.repo;


import ex3.demo.beans.DataBaseSessionMng;
import ex3.demo.filters.UserLoggingFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;


/**
 * The Filter config.
 */
@EnableWebMvc
@Configuration
public class FilterConfig implements WebMvcConfigurer {

    /**
     * The Data base session mng.
     */
    @Resource(name = "sessionMng")
    DataBaseSessionMng dataBaseSessionMng;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // apply filter only for REST controller
        registry.addInterceptor(new UserLoggingFilter(dataBaseSessionMng)).addPathPatterns("/searchUserPage" , "/ShowUserInfo"
        ,"/ShowHistoryData");
    }
}

