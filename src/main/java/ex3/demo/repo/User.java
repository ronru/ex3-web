package ex3.demo.repo;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * The User that saved in DB
 */
@Entity
public class User {

    @Id
    private long id;
    @NotBlank(message = "login Name is mandatory")
    private String loginName;
    private String userFollowers;
    private String htmlUrl;
    private int numOfSearches = 1;


    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) { this.id = id ;}

    /**
     * Gets login name.
     *
     * @return the login name
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * Sets login name.
     *
     * @param loginName the login name
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * Gets user followers.
     *
     * @return the user followers
     */
    public String getUserFollowers() {
        return userFollowers;
    }

    /**
     * Sets user followers.
     *
     * @param userFollowers the user followers
     */
    public void setUserFollowers(String userFollowers) {
        this.userFollowers = userFollowers;
    }

    /**
     * Gets html url.
     *
     * @return the html url
     */
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * Sets html url.
     *
     * @param htmlUrl the html url
     */
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * Gets num of searches.
     *
     * @return the num of searches
     */
    public int getNumOfSearches() {
        return numOfSearches;
    }

    /**
     * Set num of searches.
     */
    public void setNumOfSearches(){
        this.numOfSearches++;
    }

    /**
     * Gets .
     *
     * @return the
     */
    public long getid() {
        return id;
    }
}
