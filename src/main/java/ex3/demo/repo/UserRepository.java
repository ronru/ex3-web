package ex3.demo.repo;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * The interface User repository.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Find first 10 by order by num of searches desc list.
     *
     * @return the list
     */
    List<User>findFirst10ByOrderByNumOfSearchesDesc();


}
