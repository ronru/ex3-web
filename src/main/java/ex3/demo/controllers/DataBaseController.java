package ex3.demo.controllers;


import ex3.demo.beans.JsonParser;
import ex3.demo.repo.User;
import ex3.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


/**
 * The type Data base controller.
 */
@Controller
public class DataBaseController {
    private long userNotFound = 0 ;

    /**
     * Json parser
     */
    @Autowired
    JsonParser jsonParser;
    /**
     * This user repository is for manage the User object, here he save the user in DB
     */
    @Autowired
    UserRepository userRepository;


    /**
     * Retrieve github user name, search and save the user in the DB
     * the user id sent in session to ShowInfoUser to print user information.
     * @param userName github user name
     * @param session  session
     * @return redirect to show information
     */
    @PostMapping("/addUser")
    public String retrieveData(@RequestParam("userName") String userName ,
                               HttpSession session) {
        jsonParser.readFromUrl(userName);
        if(jsonParser.isUserFound()) {
            try {
                User user = new User();
                user.setId(jsonParser.getUserId());
                user.setLoginName(jsonParser.getLoginName());
                user.setUserFollowers(jsonParser.getUserFollowers());
                user.setHtmlUrl(jsonParser.getUserHtmlUrl());

                if (!userRepository.existsById(jsonParser.getUserId())) {
                    userRepository.save(user);
                    session.setAttribute("userId", user.getid());
                } else {
                    User tempUser = userRepository.getOne(jsonParser.getUserId());
                    tempUser.setNumOfSearches();
                    userRepository.save(tempUser);
                    session.setAttribute("userId", tempUser.getid());
                }
            } catch (TransactionSystemException msg) {
                return "/error";
            }
        }
        //user not found
        else {
          session.setAttribute("userId" , userNotFound);

        }
        return "redirect:/ShowUserInfo";

    }
}
