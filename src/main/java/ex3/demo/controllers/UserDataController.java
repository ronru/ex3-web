package ex3.demo.controllers;


import ex3.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;


/**
 * The type User data controller.
 */
@Controller
public class UserDataController {

    /**
     * The this variable is to do sign if there is user or not, if yes the details are printed else no
     */
    long notUserId = 0;

    /**
     *      * This user repository is for manage the User object, here return the user details
     */
    @Autowired
    UserRepository userRepository;
    private UserRepository getRepo() {
        return userRepository;
    }

    /**
     * Show user are charge to show the user information, if the id are not null.
     * the id received in the session.
     * @param model   the model
     * @param session the session
     * @return the object
     */
    @RequestMapping("/ShowUserInfo")
     public synchronized Object  showUserInfo(Model model, HttpSession session) {


       ModelAndView modelAndView = new ModelAndView("searchUserPage");
        try {
            long userId = (long) session.getAttribute("userId");
            if (userId != notUserId) {
              modelAndView.addObject("user", getRepo().findById(userId).orElseThrow(()
                       -> new IllegalArgumentException("Invalid user:" + userId)));
            } else {
                modelAndView.addObject("user");
                model.addAttribute("notFound", true);
            }
        } catch (NullPointerException msg) {
            System.out.println(msg.getCause());
            return "/error";
        }
      return modelAndView;
    }

    /**
     * Show user history searches
     *
     * @param model the model
     * @return historyPage
     */
    @RequestMapping("/ShowHistoryData")
    public String showUserHistory(Model model) {
        if (getRepo().findAll().size() != 0) {
            model.addAttribute("users", getRepo().findFirst10ByOrderByNumOfSearchesDesc());

        } else model.addAttribute("users");
        return "historySearches";
    }
}

