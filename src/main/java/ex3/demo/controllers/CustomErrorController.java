package ex3.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * This controller charge on error exception and return custom page
 */
@Controller
public class CustomErrorController implements ErrorController {

    /**
     * Handle error string.
     *
     * @return the string
     */
    @RequestMapping("/error")
    public String handleError() {
        return "error";
    }

    /**
     *
     * @return error page
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }


}

