package ex3.demo.controllers;

import ex3.demo.beans.DataBaseSessionMng;
import ex3.demo.Java.ResponseTransfer;
import ex3.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * This Controller delete the history searches.
 */
@Controller
public class DeleteController {

    /**
     * The Session manager delete the users from the DB
     */
    @Resource(name = "sessionMng")
    DataBaseSessionMng sessionMng;
    /**
     *
     */
    @Autowired
    UserRepository userRepository;

    private UserRepository getRepo() {
        return userRepository;
    }

    /**
     * Delete the users list from the DB
     *
     * @return the response object that contain the result if the user logout or not
     */
    @PostMapping(value = "/ClearHistory")
    public @ResponseBody
    ResponseTransfer deleteHistory() {
        ResponseTransfer user = new ResponseTransfer();
        if (sessionMng.isUserLoggedIn()) {
            try {
                getRepo().deleteAll();
                user.setUserLogOut(false);
            } catch (DataAccessException msg) {
                System.out.println(msg.getCause());
            }
        } else
            user.setUserLogOut(true);
        return user;
    }
}
