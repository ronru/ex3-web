package ex3.demo.controllers;


import ex3.demo.beans.DataBaseSessionMng;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * The type Login controller.
 */
@Controller
public class LoginController {

    /**
     * The DataBase session manager
     */
    @Resource(name = "sessionMng")
    DataBaseSessionMng dataBaseSessionMng;

    @Value("${demo.userName}")
    private String userName;

    @Value("${demo.password}")
    private String password;

    /**
     * Landing Page, if the user not logged in he will not get the main page
     *
     * @return the view search user page
     */
    @GetMapping("/")
    public String loginPage() {

        if (!dataBaseSessionMng.isUserLoggedIn())
            return "loginPage";
        return "redirect:/searchUserPage";
    }

    /**
     * Search user page is the main page, after the authentication
     *
     * @return the string
     */
    @GetMapping("/searchUserPage")
    public String searchUserPage() {
        return "searchUserPage";
    }

    /**
     * User authentication checkes if the user name and password are correct,
     * if yes the sessionDbMng init the parameter the the user logged in
     *
     * @param user         the user name
     * @param userPassword the user password
     * @param model        model for the view
     * @return redirect to login page if the user name or the password not correct
     * or redirect to main page if yes.
     */
    @PostMapping("/userAuthentication")
    public String UserAuthentication(@RequestParam("user") String user, @RequestParam("password")
            String userPassword, Model model) {

        if (user.equals(userName) && userPassword.equals(password)) {
            dataBaseSessionMng.setUserLoggedIn();
            return "redirect:/searchUserPage";
        } else {
            model.addAttribute("error", true);
            return "redirect:/";

        }
    }

    /**
     * Log out if the user pressed on log out button
     *
     * @param request do the invalidate to the session
     * @return the string
     */
    @PostMapping("logoutUser")
    public String logoutUser(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/";
    }
}

